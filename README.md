# Payload Quantity

Refer to the exhibits. The main flow contains an HTTP Request in the middle of the flow. The HTTP Listeners and HTTP Request use default configurations.  What values are accessible to the Logger at the end of the main flow after a web client submits the request to http://localhost:8080/order?color=red?

![alt text](image.png)


```
<flow name="main" doc:id="32b0c19c-fbc1-4ec8-9d84-8a46ecba1cba" >
		<http:listener doc:name="Listener" doc:id="c0e12495-4e97-4916-afc5-0ccd360f4b5b" config-ref="HTTP_Listener_config" path="/practica"/>
		<set-payload value='#["Order01"]' doc:name='"Order01"' doc:id="b63abea3-4a83-471d-b596-fee4f35fd6a7" />
		<set-variable value="1" doc:name="quantity to 1" doc:id="13a4bcc0-73dd-47a9-b69f-f4c5d30f4463" variableName="quantity"/>
		<http:request method="GET" doc:name="HTTP: POST /child" doc:id="7cd48ac3-ab47-43f0-a110-176531977e18" url="http://localhost:8081/child"/>
		<logger level="INFO" doc:name="Logger" doc:id="d3f154d7-701e-45ed-91a0-201403e4c709" message="#[payload]"/>
	</flow>
	<flow name="child" doc:id="3fe79ec5-204a-4b42-838d-c1739f0b302f" >
		<http:listener doc:name="HTTP: POST /child" doc:id="c4ba596d-8c7e-4c76-bb71-9a3b534ba8a6" config-ref="HTTP_Listener_config" path="/child"/>
		<logger level="INFO" doc:name="Logger" doc:id="29e1f6f9-3cb1-4d56-a597-d997c46b9297" message="#[payload]"/>
	</flow>
    ```

- [ ] payload
- [x] payload quantity var
- [ ] payload color query param
- [ ] payload quantity var color query param

**Respuesta correcta:** payload quantity var